<?php
/*
	Plugin Name: TLAB Calendar
	Plugin URI: https://towod.com/
	Description: Agenda mutli-accès
	Version: 0.1
	Author: Yonel Becerra
	Author URI: https://yonel-becerra.com
*/



class TlabCalendar
{

    private $regions = [
        'Île-de-France',
        'Auvergne-Rhône-Alpes',
        'Hauts-de-France',
        'Nouvelle-Aquitaine',
        'Occitanie',
        'Grand Est',
        'Provence-Alpes-Côte d\'Azur',
        'Pays de la Loire',
        'Normandie',
        'Bretagne',
        'Bourgogne-Franche-Comté',
        'Centre-Val de Loire',
        'La Réunion',
        'Guadeloupe',
        'Martinique',
        'Corse',
        'Guyane',
        'Mayotte',
    ];

	/**
	 * Constructeur
	 */
	public function __construct()
	{
	    /** ACTIVATION DU PLUGIN **/
        register_activation_hook( __FILE__, array($this, 'tlabCalendarActivate') );

        /** LOAD JS SCRIPTS */
        // On ajoute les scripts ADMIN
        add_action( 'admin_enqueue_scripts', array($this, 'tlabAddingScripts') );

        /** LOAD CSS */
        // On ajoute les CSS Admin
        add_action( 'admin_enqueue_scripts', array($this, 'tlabAddingStyles') );

	    /** CREATE POST TYPES **/
		// Création du Custom Post Type pour les Evènements
		add_action( 'init', array($this, 'createEventPostType'));
        // Création du Custom Post Type pour les Régions
        add_action( 'init', array($this, 'createRegionPostType'));

        /** CREATE TAXONOMIES **/
		// Création des catégories pour les Evènements
		add_action( 'init', array($this, 'createEventCategoryTaxonomy'), 0 );

		/** MODIFY DATAS IN INDEX OF LISTING */
		// Modification des colonnes dans le listing des Evènements
		add_filter ('manage_edit-tlab_events_columns', array($this, 'tlabEventsEditColumns'));
		// Ajout des informations dans les colonnes du listing des Evènements
		add_action ('manage_posts_custom_column', array($this, 'tlabEventsCustomColumns'));

		/** ADD METABOXES **/
		// Ajout des metabox
		add_action( 'admin_init', array($this, 'tlabEventsCreateMetaboxes') );

		/** SAVE METABOXES */
		// Sauvegarder les metaboxes
        add_action ('save_post', array($this, 'saveTlabEvents'));

        /** ADD USERS DATAS **/
        add_action('show_user_profile', array($this, 'tlabExtraUserProfileFields'));
        add_action('edit_user_profile', array($this, 'tlabExtraUserProfileFields'));

        /** SAVE USERS DATAS **/
        add_action( 'personal_options_update', array($this, 'tlabSaveExtraUserProfileFields') );
        add_action( 'edit_user_profile_update', array($this, 'tlabSaveExtraUserProfileFields') );

        /** ADD CAPATIBILITIES **/
        add_action( 'admin_init', array($this, 'tlabAddThemeCaps') );

        /** ADD OPTION PAGE **/
        add_action('admin_menu', array($this, 'plugin_settings'));

        add_action( 'admin_footer', array($this, 'ajax_javascript') );

        add_action( 'wp_ajax_generate_roles', array($this, 'generate_roles') );
        add_action( 'wp_ajax_generate_regions', array($this, 'generate_regions') );
        add_action( 'wp_ajax_remove_roles', array($this, 'remove_roles') );
        add_action( 'wp_ajax_remove_regions', array($this, 'remove_regions') );

	}

	public function tlabCalendarActivate() {

        /**
         * On ajoute les rôles utilisateur;
         */
	    $rolesToAdd = [
	            'general_administrator' => [
	                    'displayName' => 'Administrateur général',
	                    'capabilities' => [
                        ]
                    ],

	            'region_administrator' => [
                        'displayName' => 'Administrateur régional',
                        'capabilities' => []
                ],

                'region_redactor' => [
                    'displayName' => 'Rédacteur régional',
                    'capabilities' => []
                ],

                'region_member' => [
                    'displayName' => 'Membre régional',
                    'capabilities' => []
                ]
        ];

	    foreach ($rolesToAdd as $roleName => $roleDatas) {
            $addRole = add_role( $roleName, $roleDatas['displayName'], $roleDatas['capabilities'] );

            if(!$addRole) {
                wp_die('Erreur lors de l\'ajout des roles utilisateurs');
                return false;
            }

        }

    }

	/**
	 * Création du Custom Post Type "Event"
	 */
	public function createEventPostType()
	{
		// Définition des labels pour le PostType
		$labels = array(
			'name' => _x('Events', 'post type general name'),
			'singular_name' => _x('Event', 'post type singular name'),
			'add_new' => _x('Add New', 'tlab_events'),
			'add_new_item' => __('Add New Event'),
			'edit_item' => __('Edit Event'),
			'new_item' => __('New Event'),
			'view_item' => __('View Event'),
			'search_items' => __('Search Events'),
			'not_found' =>  __('No events found'),
			'not_found_in_trash' => __('No events found in Trash'),
			'parent_item_colon' => '',
		);

		// Définition de la configuration pour le PostType
		$args = array(
			'label' => 'tlab_events',
			'labels' => $labels,
			'public' => true,
			'can_export' => true,
			'show_ui' => true,
			'_builtin' => false,
			'capability_type' => 'post',
			'menu_icon' => plugins_url( 'img/icons/calendar.png', __FILE__ ),
			'hierarchical' => false,
			'rewrite' => array( "slug" => "events" ),
			'supports'=> array('title', 'thumbnail', 'excerpt', 'editor') ,
			'show_in_nav_menus' => true,
			'taxonomies' => array( 'tlab_eventcategory', 'post_tag')
		);

		// Enregistrement du PostType
		register_post_type( 'tlab_events', $args);
	}

    /**
     * Création du Custom Post Type "Region"
     */
    public function createRegionPostType()
    {
        // Définition des labels pour le PostType
        $labels = array(
            'name' => _x('Régions', 'post type general name'),
            'singular_name' => _x('Région', 'post type singular name'),
            'add_new' => _x('Add New', 'tlab_regions'),
            'add_new_item' => __('Add New Region'),
            'edit_item' => __('Edit Region'),
            'new_item' => __('New Region'),
            'view_item' => __('View Region'),
            'search_items' => __('Search Regions'),
            'not_found' =>  __('No region found'),
            'not_found_in_trash' => __('No regions found in Trash'),
            'parent_item_colon' => '',
        );

        // Définition de la configuration pour le PostType
        $args = array(
            'label' => __('Régions'),
            'labels' => $labels,
            'public' => true,
            'can_export' => true,
            'show_ui' => true,
            '_builtin' => false,
            'capability_type' => 'post',
            'menu_icon' => plugins_url( 'img/icons/map-marker.png', __FILE__ ),
            'hierarchical' => false,
            'rewrite' => array( "slug" => "events" ),
            'supports'=> array('title') ,
            'show_in_nav_menus' => true
        );

        // Enregistrement du PostType
        register_post_type( 'tlab_regions', $args);
    }

	/**
	 * Création de la taxonomy pour les catégories d'évènements
	 */
	public function createEventCategoryTaxonomy()
	{
		// Définition des labels pour la Taxonomy
		$labels = array(
			'name' => _x( 'Categories', 'taxonomy general name' ),
			'singular_name' => _x( 'Category', 'taxonomy singular name' ),
			'search_items' =>  __( 'Search Categories' ),
			'popular_items' => __( 'Popular Categories' ),
			'all_items' => __( 'All Categories' ),
			'parent_item' => null,
			'parent_item_colon' => null,
			'edit_item' => __( 'Edit Category' ),
			'update_item' => __( 'Update Category' ),
			'add_new_item' => __( 'Add New Category' ),
			'new_item_name' => __( 'New Category Name' ),
			'separate_items_with_commas' => __( 'Separate categories with commas' ),
			'add_or_remove_items' => __( 'Add or remove categories' ),
			'choose_from_most_used' => __( 'Choose from the most used categories' ),
		);

		// Définition de la configuration pour la Taxonomy
		$args = array(
			'label' => __('Event Category'),
			'labels' => $labels,
			'hierarchical' => true,
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => array( 'slug' => 'event-category' ),
		);

		// Enregistrement de la Taxonomy
		register_taxonomy('tlab_eventcategory','tlab_events', $args);
	}

	/**
	 * Modifier les colonnes dans le listing des évènements
	 */
	public function tlabEventsEditColumns($columns) {
 		
 		// Tableau contenant l'identifiant de chaque colonne et son titre
		$columns = array(
			"cb" => "<input type=\"checkbox\" />",
			"tlab_col_ev_thumb" => "Thumbnail",
			"title" => "Event",
			"tlab_col_ev_cat" => "Category",
			"tlab_col_ev_date" => "Dates",
			"tlab_col_ev_times" => "Times",
			"tlab_col_ev_desc" => "Description",
			);

		return $columns;
	}

	/**
	 * Placer les informations concernant l'évènement dans les colonnes du listing des évènements
	 */
	public function tlabEventsCustomColumns	($column)
	{
		global $post;

		// On récupère les customs fields de chaque évènement
		$custom = get_post_custom();

		switch ($column)
		{
			// Catégorie de l'évènement
			case "tlab_col_ev_cat":
				// - show taxonomy terms -
				$eventcats = get_the_terms($post->ID, "tlab_eventcategory");
				$eventcats_html = array();

				if ($eventcats) {
					foreach ($eventcats as $eventcat)
						array_push($eventcats_html, $eventcat->name);
					echo implode($eventcats_html, ", ");
				} else {
					_e('None', 'themeforce');;
				}
			break;

			// Dates de l'évènement
			case "tlab_col_ev_date":
				$startd = $custom["tlab_events_startdate"][0];
				$endd = $custom["tlab_events_enddate"][0];
				$startdate = date("F j, Y", $startd);
				$enddate = date("F j, Y", $endd);
				echo $startdate . '<br /><em>' . $enddate . '</em>';
			break;

			// Heures de l'évènement
			case "tlab_col_ev_times":
				$startt = $custom["tlab_events_startdate"][0];
				$endt = $custom["tlab_events_enddate"][0];
				$time_format = get_option('time_format');
				$starttime = date($time_format, $startt);
				$endtime = date($time_format, $endt);
				echo $starttime . ' - ' .$endtime;
			break;

			// Miniature de l'évènement
			case "tlab_col_ev_thumb":
				$post_image_id = get_post_thumbnail_id(get_the_ID());
				if ($post_image_id) {
					$thumbnail = wp_get_attachment_image_src( $post_image_id, 'post-thumbnail', false);

					if ($thumbnail) (string)$thumbnail = $thumbnail[0];

					echo '<img src="';
					echo bloginfo('template_url');
					echo '/timthumb/timthumb.php?src=';
					echo $thumbnail;
					echo '&h=60&w=60&zc=1" alt="" />';
				}
			break;

			// Description de l'évènement
			case "tlab_col_ev_desc";
				the_excerpt();
			break;

		}
	}
	/**
	 * Ajout des metaboxes dans le custom post type
	 */
	public function tlabEventsCreateMetaboxes()
	{
		add_meta_box('tlab_events_meta', 'Events', array($this, 'tlabEventsMeta'), 'tlab_events');
	}

	/**
	 * Affichage de la metabox des évènements
	 */
	public function tlabEventsMeta()
	{
		global $post;



		$custom = get_post_custom($post->ID);

        $meta_sd = null;

		if(isset($custom["tlab_events_startdate"]) && isset($custom["tlab_events_enddate"])) {
            $meta_sd = $custom["tlab_events_startdate"][0];
            $meta_ed = $custom["tlab_events_enddate"][0];
            $meta_st = $meta_sd;
            $meta_et = $meta_ed;
        }


		// - grab wp time format -

		//$date_format = get_option('date_format'); // Not required in my code
		// $time_format = get_option('time_format');

		// - populate today if empty, 00:00 for time -

		if ($meta_sd == null) { $meta_sd = time(); $meta_ed = $meta_sd; $meta_st = 0; $meta_et = 0;}

		// - convert to pretty formats -

		$clean_sd = date("m/d/Y", $meta_sd);
		$clean_ed = date("m/d/Y", $meta_ed);
		$clean_st = date("H:i", $meta_st);
		$clean_et = date("H:i", $meta_et);

        // Préparation de la query pour la liste des régions
        $regionsLoop = new WP_Query( array( 'post_type' => 'tlab_regions', 'posts_per_page' => -1 ) );

		// - security -

		echo '<input type="hidden" name="tlab-events-nonce" id="tlab-events-nonce" value="' .
		wp_create_nonce( 'tlab-events-nonce' ) . '" />';

		// - output -
        $currentRegion = get_post_meta($post->ID, 'tlab_region_id');

        if(empty($currentRegion[0])) {
            $currentRegion = get_user_meta(get_current_user_id(), 'tlab_region_id');
        }
		?>
		<div class="tlab-meta">
			<ul>
                <!-- Si l'utilisateur est un administrateur général ou d'une région -->
                <?php

                    $currentUser = wp_get_current_user();
                    $currentUserRole = '';
                    foreach ($currentUser->caps as $role => $v) {
                        $currentUserRole = $role;
                    }

                    if($currentUserRole == 'general_administrator' || $currentUserRole == 'region_administrator' ) {
                ?>
                <li>
                    <label for="tlab_region_id">Dans quelle région souhaitez-vous ajouter cet évènement?</label><br>
                    <select class="regular-text" type="text" name="tlab_region_id" id="tlab_region_id">
                        <?php

                            $selected = '';

                            // Boucle sur les régions pour lister toutes les régions possibles
                            while( $regionsLoop->have_posts() ) : $regionsLoop->the_post();

                            $selected = '';


                            if($currentRegion[0] == get_the_ID()) {
                                $selected = 'selected';
                            }

                            ?>

                            <option class="regular-text" type="text" value="<?php echo get_the_ID(); ?>" <?php echo $selected; ?> ><?php echo get_the_title(); ?></option>

                        <?php endwhile; wp_reset_query(); ?>
                    </select>
                    <hr>
                </li>
                        <?php } // End -- Fin si c'est un administrateur
                        elseif($currentUserRole == 'region_redactor') {

                            $user_region = get_user_meta($currentUser->ID, 'tlab_region_id');

                            echo '<input type="hidden" name="tlab_region_id" id="tlab_region_id" value="' .
                                $user_region[0] . '" />';
                        }
                        ?>
				<li>
					<label>A quelle date débute cet évènement? </label><br>
                    <input name="tlab_events_startdate" class="tlabdate" value="<?php echo $clean_sd; ?>" />
				</li>

				<li>
					<label>A quelle heure débute cet évènement? </label><br>
                    <input name="tlab_events_starttime" value="<?php echo $clean_st; ?>" />
				    <hr>
                </li>

				<li>
					<label>A quelle date se termine cet évènement? </label><br>
                    <input name="tlab_events_enddate" class="tlabdate" value="<?php echo $clean_ed; ?>" />
				</li>

				<li>
				<label>A quelle heure se termine cet évènement? </label><br>
                    <input name="tlab_events_endtime" value="<?php echo $clean_et; ?>" />
				</li>
			</ul>
		</div>
		<?php
	}

	public function tlabAddingScripts() {

		wp_register_script('admin_events_js', plugins_url('/js/admin_events.js', __FILE__), array('jquery', 'jquery-ui-datepicker'),'1.0.0', true);
		
		// Administration
		if(is_admin()) {

			global $post_type;
			// -----------------------
			// Event PostType Page & Listing
			if($post_type == 'tlab_events')
			{
				wp_enqueue_script('admin_events_js');
                wp_enqueue_script( 'bootstrap', 'http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', array('jquery'), null, true);
			}
			
		}
	}

	public function tlabAddingStyles() {


        wp_enqueue_style( 'bootstrap', 'http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css' );
		// Administration
		if(is_admin()) {

			global $post_type;
			// -----------------------
			// Event PostType Page & Listing
			if($post_type == 'tlab_events')
			{


			}

		}
	}

	public function saveTlabEvents()
    {
        global $post;


        // On vérifie que la demande provient bien du formulaire créé
        if (!isset($_POST['tlab-events-nonce']) || !wp_verify_nonce($_POST['tlab-events-nonce'], 'tlab-events-nonce')) {
            return get_the_ID();

        }


        // On vérifie que l'utilisateur peut modifier l'évènement
        if (!current_user_can('edit_post', $post->ID)) {
            return $post->ID;
        }


        // - convert back to unix & update post

        if (!isset($_POST["tlab_events_startdate"])) {
            return $post;
        }


        $updatestartd = strtotime( $_POST["tlab_events_startdate"] . $_POST["tlab_events_starttime"] );
        update_post_meta($post->ID, "tlab_events_startdate", $updatestartd);


        if (!isset($_POST["tlab_events_enddate"])){
            return $post;
        }

        $updateendd = strtotime ( $_POST["tlab_events_enddate"] . $_POST["tlab_events_endtime"]);
        update_post_meta($post->ID, "tlab_events_enddate", $updateendd );

        update_post_meta($post->ID, "tlab_region_id", $_POST['tlab_region_id'] );

    }

    /**
     * Ajout des informations dans le profil utilisateur
     */
    public function tlabExtraUserProfileFields($user) {

        $regionsLoop = new WP_Query( array( 'post_type' => 'tlab_regions', 'posts_per_page' => -1 ) );

        ?>

        <h3>Titre du bloc d'information</h3>
        <table class="form-table">
            <tbody>
            <tr>
                <th>Région de l'utilisateur</th>
                <td>
                    <select class="regular-text" id="tlab_region" type="text" name="tlab_region">
                        <?php

                            $selected = '';

                            // Boucle sur les régions pour lister toutes les régions possibles
                            while( $regionsLoop->have_posts() ) : $regionsLoop->the_post();

                            $selected = '';


                            if(get_the_author_meta('tlab_region_id', $user->ID ) == get_the_ID()) {
                                $selected = 'selected';
                            }

                            ?>

                            <option class="regular-text" type="text" value="<?php echo get_the_ID().'" '.$selected; ?> ><?php echo get_the_title(); ?></option>

                        <?php endwhile; wp_reset_query();
                        ?>
                    </select>
                </td>
            </tr>
        </table>

        <?php

     }

     /**
      * Sauvegarde des informations d'utilisateur
      */
     public function tlabSaveExtraUserProfileFields( $user_id ) {

         //wp_die(print_r($_POST));

         if ( !current_user_can( 'edit_user', $user_id ) ) {
             return false;
         }

         update_user_meta( $user_id, 'tlab_region_id', $_POST['tlab_region'] );

     }

     /**
      * Ajout des possibilités pour les utilisateurs
      */
    function tlabAddThemeCaps() {
        // gets the administrator role
        $region_redactor = get_role( 'region_redactor' );

        $region_redactor->add_cap( 'edit_posts' );
    }

    /** Ajoute la page de menu pour notre plugin **/
    function plugin_settings(){
        add_menu_page( 'Calendar', 'Calendrier', 'manage_options', 'calendar', array($this, 'calendar_init') );
    }

    function calendar_init(){

        include(dirname(__FILE__).'/templates/options.php');
    }

    function ajax_javascript() {
        $script = '<script type="text/javascript" >
            function generateRoles() {
    
                var data = {
                    "action": "generate_roles"
                };
    
                // since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
                jQuery.post(ajaxurl, data, function(response) {
                    alert(response);
                });
            }
            
            function generateRegions() {
    
                var data = {
                    "action": "generate_regions"
                };
    
                // since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
                jQuery.post(ajaxurl, data, function(response) {
                    alert(response);
                });
            }
            
            function removeRoles() {
    
                var data = {
                    "action": "remove_roles"
                };
    
                // since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
                jQuery.post(ajaxurl, data, function(response) {
                    alert(response);
                });
            }
            
            function removeRegions() {
    
                var data = {
                    "action": "remove_regions"
                };
    
                // since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
                jQuery.post(ajaxurl, data, function(response) {
                    alert(response);
                });
            }
        </script>';

        echo $script;
    }

    function generate_regions() {

        global $wpdb; // this is how you get access to the database



        foreach ($this->regions as $k => $v) {

            // Create post object
            $region = array(
                'post_title'    => $v,
                'post_status'   => 'publish',
                'post_type'     => 'tlab_regions'
            );

            if(!$this->region_exist($v)) {
                // Insert the post into the database
                $inserted = wp_insert_post( $region );

                if($inserted) {
                    echo 'La région '.$v.' a bien été enregistrée'."\r\n";
                } else {
                    echo '# La région '.$v.' n\'a pas été enregistrée'."\r\n";
                }
            } else {
                echo '# La région '.$v.' existe déjà'."\r\n";
            }


        }


        wp_die(); // this is required to terminate immediately and return a proper response
    }

    function generate_roles() {
        global $wpdb; // this is how you get access to the database

        $args_regions = array('post_type'=>'tlab_regions', 'post_statut'=> 'publish', 'posts_per_page' => -1);
        $q = new WP_Query($args_regions);

        if ($q->have_posts()) {
            //début de la boucle

            while ($q->have_posts()) : $q->the_post();

                $rolesPrefixes = [
                    'DSC_'  => 'DSC - ',
                    'DS_'   => 'DS - ',
                    'ELU_'   => 'Élu - ',
                    'SYN_'   => 'Syndiqué - '
                ];

                foreach ($rolesPrefixes as $k => $v) {
                    if(!$this->role_exists($k . get_the_ID()))
                    {
                        add_role($k . get_the_ID(), $v . get_the_title(), '');
                        echo 'Le rôle : '.$k . get_the_ID().' : a bien été généré'."\r\n";
                    } else {
                        echo 'Le rôle : '.$k . get_the_ID().' : existe déja'."\r\n";
                    }

                }



            endwhile;
        } else {
            echo 'Les rôles n\'ont pas été enregistrées.';
        }



        wp_die(); // this is required to terminate immediately and return a proper response
    }

    function remove_roles() {
        global $wpdb; // this is how you get access to the database

        $args_regions = array('post_type'=>'tlab_regions', 'post_statut'=> 'publish', 'posts_per_page' => -1);
        $q = new WP_Query($args_regions);

        if ($q->have_posts()) {
            //début de la boucle

            while ($q->have_posts()) : $q->the_post();

                $rolesPrefixes = [
                    'DSC_'  => 'DSC - ',
                    'DS_'   => 'DS - ',
                    'ELU_'   => 'Élu - ',
                    'SYN_'   => 'Syndiqué - '
                ];

                foreach ($rolesPrefixes as $k => $v) {

                    for($i=0;$i<200;$i++) {

                        if($this->role_exists($k . $i))
                        {
                            remove_role($k . $i);

                            echo 'Le rôle : ' . $k . $i . ' : a bien été supprimé' . "\r\n";
                        }
                    }
                }



            endwhile;
        } else {
            echo 'Les rôles n\'ont pas été supprimés.';
        }



        wp_die(); // this is required to terminate immediately and return a proper response
    }



    function remove_regions() {

        global $wpdb; // this is how you get access to the database



        foreach ($this->regions as $k => $v) {

            $region = $this->region_exist($v);

            if($region) {
                // Insert the post into the database
                wp_delete_post( $region->ID );

                echo 'La région '.$v.' a bien été supprimée'."\r\n";

            } else {
                echo '# La région '.$v.' n\'existe pas'."\r\n";
            }


        }


        wp_die(); // this is required to terminate immediately and return a proper response
    }




    public function role_exists( $role ) {

        if( ! empty( $role ) ) {
            return $GLOBALS['wp_roles']->is_role( $role );
        }

        return false;
    }

    public function region_exist( $region ) {

        if( ! empty( $region ) ) {

            $exist = get_page_by_title($region, OBJECT, 'tlab_regions');

            if($exist) {
                return $exist;
            }
        }

        return false;
    }


}

new TlabCalendar();

