<div class="tlab_settings">

    <h1>Paramètres du Calendrier</h1>

    <div class="row">
        <div class="col-sm-9">
            <h2>Installation</h2>

            <div class="btn-group">
                <button class="btn btn-primary" onclick="javascript: generateRegions()">1. Générer toutes les régions</button>
                <button class="btn btn-primary" onclick="javascript: generateRoles()">2. Générer les rôles utilisateurs</button>
                <button class="btn btn-primary">3. Installer le contenu de démonstration</button>
            </div>

            <div class="btn-group">
                <button class="btn btn-danger" onclick="javascript: removeRegions()">Supprimer les régions</button>
                <button class="btn btn-danger" onclick="javascript: removeRoles()">Supprimer les rôles utilisateurs</button>
                <button class="btn btn-danger">Supprimer le contenu de démonstration</button>
            </div>

        </div>
        <div class="col-sm-3">
            <p>test</p>
        </div>
    </div>
</div>